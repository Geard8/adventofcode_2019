adventofcode_2019 for Hands-on BDD study.

All following command is run from the project root folder.

Install requirements with command
"pip install -r requirements.txt"

Run test with command.
"pytest --cov-report=html --cov=. --cov-report term"

Result from test is saved in folder coverage when test is run 
with "pytest --cov-report=html --cov=. --cov-report term" and can be view by opening the file "index.html"

Run a specic day with command
python3 day_x/day_x.py
Where x is the day you want to run.

![coverage](https://gitlab.com/Geard8/adventofcode_2019/badges/main/coverage.svg?job=unit-test)

-----------------------------------------------------------------------------------------

reflections about your experience using TDD vs BDD

Jag har svårt att göra en ordenligt bedömning av BDD eftersom stor nytta i det ligger möjligheten och flexibilitet att utveckla senarion tillsammans många person som även inkluderar personer som inte är tekniskt kunskap inom vad som behöves för att utveckla mjukvaran, då jag arbetar ensam så har jag inte haft möjlighet att uppleva den fördelen.

En bra sak med TDD är att lösa problem igenom att skriva tester först ändra ens sätt att tänka på vad som behöves för att göra något, vilket kan leta till bättre lösningar och även potensielt spara tid över projektet då det blir mindra fel som dyker upp senare då man undviker eller upptänker felen när man utvecklar koden. En viktig sak är att vara någran med testern för dåliga tester kan skapa mer problem än det gör nytta.

BDD har samma fördel, men efter som det har fler saker som behöves göras så blir det mer tidkrävan och där med större chans att det tar mer tid än man skulle vilja. Detta är något jag framförallt känt när jag jobbart ensam, då har BDD känts mer som en begärnsing som tar mer tid utan att det blir bättre. Samt efter som det kommer med fler steg som andra delar beror på så behöver man vara väldigt uppmarksam att senarion är bra för annas blir det lätt att man skriva tester och sen kod som inte blir bra eller rätt då senariot är otydlig eller focuserar på fel saker vilket leder till dåliga tester och dålig kod. Förhoppnings viss upptäker man att tester och koden är inte tillräkligt bra och behöves göras om, men efter som det är fler steg med senario, test och utveckling av kod så är det så många saker som behöves göras om eller anpassa till det bedöms vara tillräkligt bra. 
