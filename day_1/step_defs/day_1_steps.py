from ..day_1 import get_fuel_required_for_mass_part1, get_data, get_sum_fuel_requirements, main, get_fuel_required_for_mass_part2
from pytest_bdd import given, when, then, parsers

@given(parsers.parse('module is of {mass:d} mass'), target_fixture="mass")
def given_mass(mass):
    return mass

@when(parsers.parse('mass is used to determined amount of fuel required'), target_fixture="fuel_required")
def determined_fuel(mass):
    return get_fuel_required_for_mass_part1(mass)

@then(parsers.parse('required amount of fuel is {fuel:d} for that module'))
def check_fuel_required_is_correct_amount(fuel_required, fuel):
    assert fuel_required == fuel


@given(parsers.parse('you get data from data.txt file'), target_fixture="data")
@given(parsers.parse('data based on puzzle input from AoC 2019 day 1'), target_fixture="data")
def given_data():
    return get_data("data.txt")

@then(parsers.parse('data will exist'))
def check_data_exist(data):
    assert data is not False

@when(parsers.parse('using data to get an answer'), target_fixture="answer")
def get_answer_with_data(data):
    return get_sum_fuel_requirements(data)

@then(parsers.parse('answer is something'))
def check_answer_exist(answer):
    assert answer is not False

@given(parsers.parse('day 1 script are used.'), target_fixture="respons")
def call_main_of_day_1():
    return main()

@then(parsers.parse('it shall give a positive respons.'))
def check_for_positive_respons(respons):
    assert respons is True

@when(parsers.parse('mass is used to determined amount of fuel required in a way that account for the mass of the fuel'), target_fixture="fuel_required")
def step_function(mass):
    return get_fuel_required_for_mass_part2(mass)

@when(parsers.parse('using data to get an answer that correct for fuel mass.'), target_fixture="answer")
def get_answer_with_data(data):
    return get_sum_fuel_requirements(data, True)
