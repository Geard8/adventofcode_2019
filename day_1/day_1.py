
def get_data(file_name):
    """
    Retrun preperad data based on str file_name.

    param: 
        file_name: str for what file name will be used to get data from
    return: list with all data from file with name based on file_name
    """
    with open(f"day_1/{file_name}", 'r') as f:
        lines = f.readlines()
    data_puzzle_input = [int(e.strip()) for e in lines]
    return data_puzzle_input


def get_fuel_required_for_mass_part1(mass):
    """
       Need one int value that is a number representing a module mass.
    Then it will calculate the fuel needed for that module and return what value as an int.
    Calculate fuel is done by:
        Specifically, to find the fuel required for a module, take its mass, divide by three, round down, and subtract 2.
    param: 
        mass: int
    return: int that is fuel required for a module based on mass
    """
    return (mass // 3) - 2

def get_fuel_required_for_mass_part2(mass):
    """
       Need one int value that is a number representing a module mass.
    Then it will calculate the fuel needed for that module and then calculate fuel needed 
    to cover the fuel and return what the fuel needed as an int.
    Calculate fuel for module is done by:
        Specifically, to find the fuel required for a module, take its 
        mass, divide by three, round down, and subtract 2.
    Calculate fuel for all the fuel is done by:
        So, for each module mass, calculate its fuel and add it to the total. Then, treat 
        the fuel amount you just calculated as the input mass and repeat 
        the process, continuing until a fuel requirement is zero or negative.
    param: 
        mass: int
    return: int that is fuel required for a module based on mass
    """

    module_fuel = get_fuel_required_for_mass_part1(mass)
    fuel_for_latest_amount_of_fuel = get_fuel_required_for_mass_part1(module_fuel)

    fuel_to_cover_fuel = 0 # declare fuel_to_cover_fuel to exist and be used.
    if fuel_for_latest_amount_of_fuel > 0:
        fuel_to_cover_fuel = module_fuel + fuel_for_latest_amount_of_fuel
    else:
        fuel_to_cover_fuel = module_fuel

    while fuel_for_latest_amount_of_fuel > 0:
        fuel_for_latest_amount_of_fuel = get_fuel_required_for_mass_part1(fuel_for_latest_amount_of_fuel)
        if fuel_for_latest_amount_of_fuel > 0:
            fuel_to_cover_fuel += fuel_for_latest_amount_of_fuel

    return fuel_to_cover_fuel

def get_sum_fuel_requirements(data, correct_for_fuel = False):
    """
       Take list of module mass and calculate what fuel required they have and return the sum them all.
    param: 
        data: list with int of mass for all module in data list.
    return: int that is fuel required for all module based on mass in data
    """

    sum = 0
    for mass in data:
        if correct_for_fuel:
            sum += get_fuel_required_for_mass_part2(mass)
        else:
            sum += get_fuel_required_for_mass_part1(mass)

    print(f"fuel required for all modules is: {sum}")
    return sum

def main():
    """ 
    use data from file data.txt and get an answer.
    File data.txt most be in day_1 folder.

    return: True if all when well otherwise False.
    """
    data_puzzle_input = get_data("data.txt")
    success_part1 = get_sum_fuel_requirements(data_puzzle_input)
    success_part2 = get_sum_fuel_requirements(data_puzzle_input, True)

    return bool(success_part1) and bool(success_part2)

if __name__ == "__main__":
    main()
