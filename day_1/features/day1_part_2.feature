
Scenario Outline: AoC 2019 day 1 part 2 example Scenario
    Given module is of <mass> mass
    When mass is used to determined amount of fuel required in a way that account for the mass of the fuel
    Then required amount of fuel is <fuel> for that module

    Examples:
        | mass | fuel |
        |  14   |  2  |
        |  1969   |  966  |
        |  100756   |  50346  |

Scenario Outline: AoC 2019 day 1 part 2 check for a answer Scenario
    Given data based on puzzle input from AoC 2019 day 1
    When using data to get an answer that correct for fuel mass.
    Then answer is something
