
Scenario Outline: AoC 2019 day 1 part 1 example Scenario
    Given module is of <mass> mass
    When mass is used to determined amount of fuel required
    Then required amount of fuel is <fuel> for that module

    Examples:
        | mass | fuel |
        |  12   |  2  |
        |  14   |  2  |
        |  1969   |  654  |
        |  100756   |  33583  |

Scenario Outline: get data Scenario
    Given you get data from data.txt file
    Then data will exist

Scenario Outline: AoC 2019 day 1 part 1 check for a answer Scenario
    Given data based on puzzle input from AoC 2019 day 1
    When using data to get an answer
    Then answer is something

Scenario Outline: check day 1 is running correctly.
    Given day 1 script are used.
    Then it shall give a positive respons.
