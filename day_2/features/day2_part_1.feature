
Scenario Outline: get data Scenario
    Given you get data from data.txt file
    Then data will exist

Scenario Outline: use an opcode
    Given a opcode
    Then a opcode will exist

Scenario Outline: use list in opcode
    Given a opcode with an number list
    When first number in list is changed to 1
    Then first number in list will be 1

Scenario Outline: use opcode to run AoC 2019 day 2 part 1 example
    Given a opcode with an number list <start_list>
    When running Intcode
    Then number list will be <end_list>

    Examples:
        | start_list | end_list |
        |  1,0,0,0,99   |  2,0,0,0,99  |
        |  2,3,0,3,99   |  2,3,0,6,99  |
        |  2,4,4,5,99,0   |  2,4,4,5,99,9801  |
        |  1,1,1,4,99,5,6,0,99   |  30,1,1,4,2,5,6,0,99  |

Scenario Outline: run opcode to get puzzle answer for part 1
    Given a opcode with an number list based on puzzle input
    And number list is updated based on AoC 2019 day 2 part 1 instruction
    When running Intcode
    Then opcode will have an answer for part 1 instruction

Scenario Outline: check day 2 is running correctly.
    Given day 2 script are used.
    Then it shall give a positive respons.
