Scenario Outline: run opcode to get puzzle answer for part 2
    Given a opcode with an number list based on puzzle input
    When getting an answer for part 2
    Then opcode will have an answer for part 2
