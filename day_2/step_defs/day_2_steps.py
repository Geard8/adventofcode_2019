from ..day_2 import *
from pytest_bdd import given, when, then, parsers

@given(parsers.parse('you get data from data.txt file'), target_fixture="data")
def given_data():
    return get_data("data.txt")

@then(parsers.parse('data will exist'))
def check_data_exist(data):
    assert data is not False

@given(parsers.parse('a opcode'), target_fixture="opcode")
def get_empty_opcode():
    return OpCode()

@then(parsers.parse('a opcode will exist'))
def is_opcode(opcode):
    assert isinstance(opcode, OpCode) is True

@given(parsers.parse('a opcode with an number list'), target_fixture="opcode")
def get_opcode_with_int_list():
    return OpCode([2,3,0,3,99])

@when(parsers.parse('first number in list is changed to 1'))
def change_first_int_to_1(opcode):
    opcode.change_int_in_list(1, 0)

@then(parsers.parse('first number in list will be 1'))
def check_first_in_is_1(opcode):
    assert opcode.int_list[0] == 1

@given(parsers.parse('a opcode with an number list {start_list}'), target_fixture="opcode")
def create_opcode_with_list(start_list):
    int_list = list(map(int, start_list.split(",")))
    return OpCode(int_list)

@when(parsers.parse('running Intcode'))
def run_intcode(opcode):
    opcode.intcode()

@then(parsers.parse('number list will be {end_list}'))
def is_opcode_correct(end_list, opcode):
    int_list = list(map(int, end_list.split(",")))
    assert opcode.int_list == int_list

@given(parsers.parse('a opcode with an number list based on puzzle input'), target_fixture="opcode")
def get_opcode_based_on_puzzle_input():
    return OpCode(get_data("data.txt"))

@given(parsers.parse('number list is updated based on AoC 2019 day 2 part 1 instruction'))
def update_int_list_with_instruction(opcode):
    opcode.change_int_in_list(12, 1)
    opcode.change_int_in_list(2, 2)

@then(parsers.parse('opcode will have an answer for part 1 instruction'))
def check_an_answer_exist(opcode):
    assert opcode.get_answer_part_1() >= 0

@given(parsers.parse('day 2 script are used.'), target_fixture="respons")
def call_main_of_day_2():
    return main()

@then(parsers.parse('it shall give a positive respons.'))
def check_for_positive_respons(respons):
    assert respons is True

@when(parsers.parse('getting an answer for part 2'), target_fixture="answer")
def get_answer_part2(opcode):
    return opcode.get_answer_part_2()

@then(parsers.parse('opcode will have an answer for part 2'))
def step_function(answer):
    assert answer > 0
