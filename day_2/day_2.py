
def get_data(file_name):
    """
    Retrun preperad data based on str file_name.

    param: 
        file_name: str for what file name will be used to get data from
    return: list with all data from file with name based on file_name
    """
    with open(f"day_2/{file_name}", 'r') as f:
        line = f.readline()
    data_puzzle_input = list(map(int, line.split(",")))

    return data_puzzle_input

class OpCode():
    def __init__(self, int_list=[]):
        self.int_list = int_list
    
    def change_int_in_list(self, change_to, change_position=0):
        """
        Change one value in int_list at a specific position.

        param: 
            change_to: int of what to change to.
            change_position: int of what position will be change.
        return: Bool
        """
        self.int_list[change_position] = change_to
        return True

    def intcode(self):
        """
        An Intcode program is a list of integers separated by commas 
        (like 1,0,0,3,99). To run one, start by looking at the first integer 
        (called position 0). Here, you will find an opcode - either 1, 2, or 99. 
        The opcode indicates what to do; for example, 99 means that the program is 
        finished and should immediately halt. Encountering an unknown opcode 
        means something went wrong.

        Opcode 1 adds together numbers read from two positions and stores the 
        result in a third position. The three integers immediately after the 
        opcode tell you these three positions - the first two indicate the 
        positions from which you should read the input values, and the third 
        indicates the position at which the output should be stored.

        For example, if your Intcode computer encounters 1,10,20,30, it should 
        read the values at positions 10 and 20, add those values, and then 
        overwrite the value at position 30 with their sum.

        Opcode 2 works exactly like opcode 1, except it multiplies the two inputs 
        instead of adding them. Again, the three integers after the opcode indicate 
        where the inputs and outputs are, not their values.

        Once you're done processing an opcode, move to the next one by 
        stepping forward 4 positions.

        return: Bool
        """

        current_index = 0 # start at index and keep track of current index is running.
        try:
            while True:
                if self.int_list[current_index] == 1:
                    first_index = self.int_list[current_index + 1]
                    secend_index = self.int_list[current_index + 2]
                    change_to = self.int_list[first_index] + self.int_list[secend_index]
                    change_position = self.int_list[current_index + 3]
                    self.change_int_in_list(change_to, change_position)
                
                elif self.int_list[current_index] == 2:
                    first_index = self.int_list[current_index + 1]
                    secend_index = self.int_list[current_index + 2]
                    change_to = self.int_list[first_index] * self.int_list[secend_index]
                    change_position = self.int_list[current_index + 3]
                    self.change_int_in_list(change_to, change_position)
                
                elif self.int_list[current_index] == 99:
                    return True
                
                current_index += 4
        except:
            return False

    def get_answer_part_1(self):
        return self.int_list[0]

    def get_answer_part_2(self):
        """
        Find answer for part 2 by changeing int_list index 1 and 2 to every combination 
        where they are 0-99 and finding what combination 
        gives the answer 19690720 in index 0 after running intcode.
        Index 1 will be called noun and index 2 till be called verb
        Answer for part 2 is made by 100 * noun + verb

        return: int that is answer for part 2
        """

        int_list_backup = self.int_list.copy() # to be used to rest int_list

        # noun & verb is terminaligy from AoC for the changeing value to use for 2019 day 2 part 2
        for noun in range(0, 99):
            for verb in range(0, 99):
                self.change_int_in_list(noun, 1)
                self.change_int_in_list(verb, 2)
                # print(f"{noun}, {verb}") # TEST CODE
                # print(self.int_list) # TEST CODE
                is_intcode_success = self.intcode()
                # print(self.get_answer_part_1()) # TEST CODE
                if not is_intcode_success:
                    self.int_list = int_list_backup.copy() # rest int_list
                    continue
                elif self.get_answer_part_1() == 19690720:
                    return 100 * noun + verb
                else:
                    self.int_list = int_list_backup.copy() # rest int_list

def main():
    """ 
    use data from file data.txt and get an answer.
    File data.txt most be in day_2 folder.

    return: True if all when well otherwise False.
    """
    data_puzzle_input = get_data("data.txt")
    opcode = OpCode(data_puzzle_input.copy())
    opcode.change_int_in_list(12, 1)
    opcode.change_int_in_list(2, 2)
    opcode.intcode()
    answer_part_1 = opcode.get_answer_part_1()
    print(f"answer to day 2 part 1 based on data.txt is : {answer_part_1}")
    success_part1 = answer_part_1 >= 0

    # rest with new opcode based in data_puzzle_input in prep for part 2
    opcode = OpCode(data_puzzle_input.copy())
    print(opcode.int_list) # TEST CODE
    answer_part_2 = opcode.get_answer_part_2()
    print(f"answer to day 2 part 2 based on data.txt is : {answer_part_2}")
    success_part2 = answer_part_2 > 0
    print(success_part2) # TEST CODE

    return bool(success_part1) and bool(success_part2)

if __name__ == "__main__":
    main()
